/* 
    Ing. Mauricio Hernández Siles
    23/01/2022
    Coding Exercise: object manipulation with NodeJs (javascript)

    Instructions: If you get stuck write the portion out you can't complete in 
    pseudo code (try to still be specific about what you would do). Include a 
    question worded how you would ask another member of the development team 
    to gain the information needed to be able to complete the task. Code 
    should be commented. Include a commented section at the top with your name,
    date of development, and purpose of script.

    Add an object for yourself following a similar format. Put each object in 
    an array so you have an array of objects (your choice how to do that). 
    Iterate through the array of objects adding the date the script was run 
    to each object as favorite things may change in the future. Console.log 
    only active records with the Name, date, Favorite movie of each person. 
    Add a function that can sort the output by object property. There may 
    be a time where no Active records are found. Include code that provides 
    a message when that occurs.
*/

let rockyObj = {
    'Name': 'Rocky',
    'Favorite Food': 'Sushi',
    'Favorite Movie': 'Back to The Future',
    'Status': 'Inactive'
}
let miroslavObj = {
    'Name': 'Miroslav',
    'Favorite Food': 'Sushi',
    'Favorite Movie': 'American Psycho',
    'Status': 'Active'
}
let donnyObj = {
    'Name': 'Donny',
    'Favorite Food': 'Singapore chow mei fun',
    'Favorite Movie': 'The Princess Bride',
    'Status': 'Inactive'
}
let mattObj = {
    'Name': 'Matt',
    'Favorite Food': 'Brisket Tacos',
    'Favorite Movie': 'The Princess Bride',
    'Status': 'Active'
}
let mauObj = {
    'Name': 'Mauricio',
    'Favorite Food': 'Hamburger',
    'Favorite Movie': 'Batman: The Dark Knight',
    'Status': 'Active'
}

let people = []
let lastIndex = 0

const validateArray = () => {
    if (people.length == 0){ // Validate if object array is empty
        console.log('Cannot work with an empty array.'); //Print error message
        return false;
    }
    return true;
}

// This method might be not very usefull for this exercise, however, I always like to make my code flexible
// enough if further on there is another specification regarding the new objects.
// i.e. if the objects also need an id or a date, then the function makes it easier to read and implement those changes.
const addObjectToArray = object => {
    object.id = lastIndex+1; // Get new index and assing it to the object
    object.createdAt = new Date(); //object creation date

    lastIndex++;
    people.push(object); // Add object to an array
}

// Update property value. Receive the id of the object to update, the property to be updated and the new value.
const updateObject = (objectId, property, newValue) => {

    const person = people.find(person => person.id == objectId); //Find the object
    
    person[property] = newValue; //Update the property value

}

const getActiveRecords = () => {

    if (validateArray()){ //validate if array is not empty
        console.log('Active records: \n');
        people.filter(person => { //filter only active objects
            if (person.Status == 'Active'){
                console.dir({ //print relevant information
                    'Name': person.Name,
                    'Created At': person.createdAt,
                    'Favorite Movie': person['Favorite Movie']})
                console.log('\n');
            }
        });
    }

}

const sortByProperty = property => {

    if(validateArray()){
        if (property in people[0]){ //valtdate if property exists in the objects
        
            console.log(`Sorted by: ${property} \n`);
        
            people
                .sort( (last,current) => last[property] > current[property] ? 1 : -1) //Sort by specific property
                .forEach(person => {
                    console.log(`Person: ${person.Name} - \t ${property}: ${person[property]}`); // print the result
                });
        }else{
            console.log("Property does nos exists.");
        }
    }


};

// Main function
const main = () => {
    
    // Add each object to the array
    addObjectToArray(rockyObj);
    addObjectToArray(miroslavObj);
    addObjectToArray(donnyObj);
    addObjectToArray(mattObj);
    addObjectToArray(mauObj);
    
    
    // Sort by object key value
    sortByProperty('Favorite Movie');
    
    // Update property value
    updateObject(2,'Favorite Movie', 'Toy Story');
    
    // Get active records
    getActiveRecords();
};

//Execution
main();

